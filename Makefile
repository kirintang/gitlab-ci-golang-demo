export VERSION=1.1.5
export ENV=prod
export PROJECT=gitlab-ci-golang-demo

PWD=$(shell pwd)
OUT_DIR=$(PWD)/cmd
OUT_NAME=$(PROJECT)_$(VERSION)
OUT_FILE=$(OUT_DIR)/$(OUT_NAME)
MAIN_FILE=main.go
PACK_FILE=$(OUT_NAME).tar.gz

build:
	@echo "移除老项目"
	@ rm -rf $(OUT_DIR)/*
	@go build -o $(OUT_FILE) $(MAIN_FILE)
	@echo "项目构建成功:" $(OUT_FILE)

run:build
	@# 执行pack前先执行一次build
	@echo "运行项目"
	@$(OUT_FILE)

pack:build
	@# 执行pack前先执行一次build
	tar -czvf $(PACK_FILE) -C $(OUT_DIR) .
	@echo "打包结束:"
	